import java.time.LocalDateTime.now
import java.time.LocalDateTime

object ClaaByName {
   def main(args: Array[String]) {
       delayed(time());
   }

   def time() = {
      println("Getting date and time")
      now()
   }
   
   def delayed( localDateTime: => LocalDateTime ) = {
      println("After evaluating function in parameter")
      println("Param is: " + localDateTime)
      localDateTime
   }
}