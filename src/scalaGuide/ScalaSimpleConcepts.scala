package scalaGuide

case class User() {
  private var _name : String = _
  
  val orders: List[Order] = Nil
  
  def name = _name
  
  def name_= (name : String): String = {
        if (name == null) {
            throw new NullPointerException("User.name cannot be null!")
        }
        _name = name;
        _name 
  }
  
  def main(args :Array[String]) : Unit = {
    
    
    println(name_=("Ramdas name"))
    
  }
  
}

case class Order() {
    var id: String = _
    var p: List[Product] = Nil
    
  // def products = orders.flatMap(o => o.products)
}

case class Product(){
  var id :String = _
  var category : String = _
}

object ScalaSimpleConcepts1 {
  val p = new Product();
  p.category = "web"
  p.id = "212"
  
  val o = new Order();
  o.id = "132654"
  o.p = List(p);
  
  val user = new User
  
  var i = 10;
  var list = List(2,2,3,4,4.8,'c')
  
  def productsByCategory(category: String) = user.orders.flatMap( o => o.p).filter(p => p.category == category)
  
  def main( args: Array[String]): Unit = {
    
    var ints = list.map { x => x.toInt }
    println("hello")
    
  }
  
}