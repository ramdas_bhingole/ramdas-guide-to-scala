package scalaGuide

import scala.util.control.Breaks
import Array._
import scala.collection.Iterator

object ScalaTpoint {
  
  final case class Symbol (name: String) {
     override def toString: String = "'" + name
  }
  
  def main(args: Array[String]){
    println(Symbol("Ramdas"))
    println("Hello Scala t point")  
    var args :  Array[String] = Array("Ramdas","BHingole")
    // Or
    //var stringArray = Array("Ramdas","Bhingole")
    
    (new VaribleDemo).main(args)
    (new Loopdemo).main(args)
    (new FunctionDemo).main(args)
    (new CollectionDemo).main(args)
    (new Employee(1,"Software Developer").main(args))
    (new TraitDemo).main(args)
  }
}

class Outer{
  class Inner{
    private def f1(){
      
    }
    protected def f2(){
      
    }
  }
  //As f1() is private it isnot allowed here. 
  //(new Inner).f1()
  
  class subclass extends Inner{
    f2()
  }
  
  class subclass2{
    // Following line shows compilation error, f2 is accesible only in subclass of f2 defined class
    
    //f2() 
  }
}

class VaribleDemo{
  //var(myvar1:Int, myvar2: String) = Pair(2,"Bhingole")
  //Or following detect datatype internally 
  var (myvar1,myvar2) = Pair(1,"Ramdas") 
  
  def main(args: Array[String]){
    myvar1 = 2 //"2" gives error internally myvar is identified as int then it cant change type 
    println(myvar2)
    
  }
} 



package society{
    package professional{
      class Executive {
        private[this] var friends = null
        private[professional] var workerDetails = null
        private[society] var secrets = null
        
        def main(args : Array[String]){
          println("Create Executive object anf call help()")
          
        }
        def help(executive: Executive){
          
          println("Executive "+ executive.secrets) // Cant access friends its private for this package
        }
      }
    }
}

//Looping section

class Loopdemo{
  def main(args:Array[String]){
    println("Hello u r in looing demo")

    println("For loop using 'to'")
    for(i <- 1 to 10){
      println(i)
    }

    println("For loop using 'until'")
    for(i <- 1 until 10){
      println(i)
    }
    
    var list = List(1,2,3,4)
    
    println("For loop using 'range' and nested for loop, and filter in for loop")
    for(i <- 1 to 3 ; j <- list if(j > 3) ){
      println("List values are "+ j)
    }
    
    println("For yield example")
    
    var retValue = for{
      j <- list if(j < 3)  
        
    }yield{
      j
    }
    
    for(i <- retValue){
      println(i)
    }
    
    println("Breakable loop break continue example")
    
    var loop = new Breaks()
    
    loop.breakable{
      for(i <- list){
        println("looping for ")
        if(i == 1)println("Breaking loop ")
        loop.break
      }
    }
  }
}

class FunctionDemo {
  def main(args : Array[String]){
    println("Hello you are in function demo")
    println("Current time methos calling by name is " + delayed(System.nanoTime()))
    
    
    println("Factorial of no is " + factorial(5))
    println("Hier order functions " + apply(layout, 10))
    println("Multiplky two numbers by using anonimous function"+ anonymousFunctionToMultiply(10,10) +anonymousFunction())
    println("Calling curried function"+ curryiedFunctionToConcat("Ramdas ")("Bhingole"))
    
  }
  
  def time() = {
      println("Getting time in nano seconds")
      System.nanoTime
   }
   def delayed( t: => Long = 152l ) = {
      println("In delayed method")
      println("Param: " + t)
      t
   }
   
  def factorial(i : Int): Int = {
    def fact(i:Int, adderTocalculateFactorial:Int) : Int = {
      
      if(i <= 1){
        adderTocalculateFactorial
      }else{
        fact(i-1, i * adderTocalculateFactorial)
      }      
      
    }
    fact(5,1)
  }
   
  def apply( f :Int => String , v:Int ) = f(v)
  def layout[A](x: A) = "[" + x.toString() + "]"
   
  var anonymousFunctionToMultiply = (x:Int, y:Int) => x * y 
  
  var anonymousFunction = () => println("Printing anonymous function")  
  
  def curryiedFunctionToConcat(s1: String)(s2: String) = s1 + s2
 
  var myMatrix = ofDim[Int](3,3)
  
}

class CollectionDemo {
  
  def main(args: Array[String]){
    println("You are in collection demo")
    println("List two dimentional list is head tail is " + twoDimList.head + twoDimList.tail)
    println("Adding two list " + List.concat(list , listDoule ))
    println("Filling data in reaped formayt" + List.fill(2, 5)("*"))
    println("Multiplication is by using tabulatin funcrion "+ multiple  + "Square is "+ squres)
    println("Reversing List order reversing hetrogenous list" + list.reverse)
    
    setOperationMethod()
    mapOperationMethod()
    toupleOperationMethod()
    optionOperationMethod()
    iteratorOperationMethod()
  }
  
  // Val and var 
  //if we can't assign value to val , It is immutable  
  // For var we can assign value to it, It is mutable
  
  // List of hetrogenious data    
  var list = List("Ramdaas","bhingole",1,true,8.5)
  
  //Two dimentional list
  var twoDimList : List[List[Int]] = List(List(1,2,3,4,5),List(1,2,3)) 
  
  val nothinglist: List[Nothing] = List()
  
  //Double data
  var listDoule : List[Double] = List(1,2,2.8)
  
  var intList = 1 :: (5 :: (6 :: Nil))
  
  var TwoDimList = (1 :: (0 :: (3 :: Nil)))::
                   (2 :: (5 :: Nil))
                   
  var squres = List.tabulate(6)( n => n * n)
  
  var multiple = List.tabulate( 5 , 6)(_ * _)
  
  
  //val array  : Array[Double] = null
  
  //listDoule.copyToArray(array , 2,5)
  var p = list.apply(2)
  //Add elements of given list to in front of this list 
  list = list.:::(List("Mr","Mrs",5.8))
  // Prepend this element to list
  list = list.+:("Mr")
  
  // Return list followed by element
  list = list.++("Hello")
  
  //Will drop first n elements
  
  listDoule.drop(2)
  
  //List contains Any data so i need to create any  
  
  val any : Any = "Ramdas"
  list.dropWhile { x => x.equals(any)}
  
  list.map { x => println("asfasf ") }
  
  def setOperationMethod(){
    var set : scala.collection.mutable.Set[Int] = scala.collection.mutable.Set(1,2,3,3,4)
    
    
    val num1 = Set(5,6,9,20,30,45)
    val num2 = Set(50,60,9,20,35,55)
    
    //Adding element in set
    set.+=(6)
    
    set.+(5)
   
    println("You are in set operation method****************")
    println("Set head and tail is "+ set.head + " " + set.tail)
    println( set.map { x => print("SetElements are " + x) })
    
    println("Collecting common values in two set"+ num1.&(num2))
    
    
  }
  
  def mapOperationMethod(){
    //Defining empty map
    var map2 = Map[String, String]()
    
    //Defining imutable map
    val map1 : Map[String, String] = Map("name" -> "Ramdas", "SNAme" -> "Bhingole")
    println("Map elements are "+ map1.seq.get("name"))
  
    map2 += ("EmployeeName" -> "Ramdas")
    
    map1.keys.foreach { x => println("AfterIteratoion map values"+ map1.get(x)) }
  
  }
  
  def toupleOperationMethod(){
    //If fixed no of elements are there then we go for touple
    
    //Touple element limit is 22
    
    println("You are in touple operation method")
    val t = (1, "hello", Console)
    
    val t2 = new Tuple3(1, "hello", Console)
    
    println("Touple elements are "+ t2._1 + t2.productArity)
    
  }
  
  def optionOperationMethod(){
  
      val capitals = Map("France" -> "Paris", "Japan" -> "Tokyo")
      
      println("capitals.get( \"France\" ) : " +  capitals.get( "France" ))
      println("capitals.get( \"India\" ) : " +  capitals.get( "India" ))
      
      println("capitals.get( \"France\" ) : " +  show(capitals.get( "France" )))
      println("capitals.get( \"India\" ) : " +  show(capitals.get( "India" )))
      
      println("capitals.get( \"France\" ) : " +  capitals.get( "France" ).getOrElse(0))
      println("capitals.get( \"India\" ) : " +  capitals.get( "India" ).getOrElse("India"))
      
      def show(x: Option[String]) = x match { 
        case Some(x) => println(x)
        case None => println("Nothing found")
      }
  }
  
  def iteratorOperationMethod(){
    println("U r in Itertor operation method")
    val map : Map[Int, String] = Map(1 -> "Ramdas",2 -> "Bhingole")
    val itr = map.iterator
    while (itr.hasNext) {
      println("Element by iterator method is "+ itr.next()._2)
    }
    
  }
  
}

class Employee(val empId : Int, val designation: String){
  //var empId = empId;
  //var designation =  designation;
  
  def showEmpData(){
    printf("Employee id %d  , Emp designation is %S ",empId, designation)
  }
  
  def main(args : Array[String]){
    showEmpData
  }
  
  class Dept(override val empId : Int, override val designation: String) extends Employee(empId,designation){

    override def main(args : Array[String]){
            showEmpData
    }
  }
  
}

//Trait behaves like abstract class
trait Equal{
  
  //Undefined method
  def isEqual(x:Any): Boolean
  //Defining method
  def isNotEqual(x: Any) : Boolean = !isEqual(x)
  
}

abstract class TraitDemo extends Equal{
  
  var x : Any
  def isEqual( x : Any) : Boolean = {
    x.isInstanceOf[TraitDemo] && 
    x.asInstanceOf[TraitDemo].x == x  
  }
  
  def main(args:Array[String]){
    println("Trait methods called")
    x= ""    
  }
  
  
}



