import scala.io.Source

object FileDemo {
  def main(args: Array[String]) {

    val text = Source
      .fromFile("C:\\Users\\r.balaji.bhingole\\scala-workspace\\DemoScalaProject\\src\\abc.txt")
      .getLines
      .flatMap { line =>
        line.split(" ")
      }.toList.groupBy { (word: String) => word }
      .mapValues(_.length)
      
    text.map(x => println("Word : " + x._1 + " Count :" + x._2))
  }

}