
case class A (name : String, rollNo:Int)

object CaseClass {
  
  def main(arg :Array[String]){
    val a = new A("Ram",1)    
    println("Name is "+ a.name)
  }
  
}